﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSchoolApp.Business;
using SimpleSchoolApp.Models;
using Moq;
using System.Data.Entity;
using System.Collections.Generic;
using SimpleSchoolApp.Data;
using SimpleSchoolApp.Constants;

namespace SimpleSchoolApp.Test.Business
{
    [TestClass]
    public class SchoolManagerTest
    {
        private SchoolManager _sut;
        
        [TestInitialize]
        public void Initialise()
        {
            
        }

        [TestMethod]
        public void AddStudentToDb_WithExisting_WillRejected()
        {
            //arrange
            var mockDataRepository = new Mock<IDataRepository>();
            mockDataRepository.Setup(d => d.CheckStudentExistsInDb(It.IsAny<Student>())).Returns(true);
            _sut = new SchoolManager(mockDataRepository.Object);

            var student = new Student{ FirstName="test", LastName="test"};
            var classId = 1;
            //act
            var result = _sut.AddStudentToClass(classId, student);
            //assert
            Assert.AreEqual(result.Status, ResponseConstants.ALREADYEXISTS);
        }

        [TestMethod]
        public void AddStudentToDb_WithNonExisting_WillSuccess()
        {
            //arrange
            var mockDataRepository = new Mock<IDataRepository>();
            mockDataRepository.Setup(d => d.CheckStudentExistsInDb(It.IsAny<Student>())).Returns(false);
            _sut = new SchoolManager(mockDataRepository.Object);

            var student = new Student { FirstName = "test", LastName = "test" };
            var classId = 1;
            //act
            var result = _sut.AddStudentToClass(classId, student);
            //assert
            Assert.AreEqual(result.Status, ResponseConstants.SUCCESS);
        }

        [TestMethod]
        public void AddStudentToDb_WithNonExistingDBRepositoryException_WillFail()
        {
            //arrange
            var mockDataRepository = new Mock<IDataRepository>();
            mockDataRepository.Setup(d => d.CheckStudentExistsInDb(It.IsAny<Student>())).Returns(false);
            mockDataRepository.Setup(d => d.AddStudentToClass(It.IsAny<Student>(), It.IsAny<int>())).Throws(new Exception());
            _sut = new SchoolManager(mockDataRepository.Object);

            var student = new Student { FirstName = "test", LastName = "test" };
            var classId = 1;
            //act
            var result = _sut.AddStudentToClass(classId, student);
            //assert
            Assert.AreEqual(result.Status, ResponseConstants.FAILURE);
        }
    }
}
