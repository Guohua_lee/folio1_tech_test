﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimpleSchoolApp.Models
{
    public class AddStudentResponse
    {
        public string Status { get; set; }
        public Student Student { get; set; }
    }
}