﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimpleSchoolApp.Models
{
    public class AddStudentRequest
    {
        public int ClassId { get; set; }
        public Student Student { get; set; }
    }
}