﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using SimpleSchoolApp.Models;
using SimpleSchoolApp.Business;
using SimpleSchoolApp.Data;
using System.Web.Http.Cors;
using SimpleSchoolApp.Constants;

namespace SimpleSchoolApp.Controllers
{
    [EnableCors("*", "*", "*")]
    public class SchoolController : ApiController
    {
        private static IDataRepository dbRepo = new DataRepository();
        private static SchoolManager schoolManager = new SchoolManager(dbRepo);

        public IHttpActionResult GetClasses()
        {
            var classes = schoolManager.GetClasses();
            return Json(classes);
        }

        public IHttpActionResult GetStudentsInClass(int id)
        {
            var students = schoolManager.GetStudentsByClass(id);
            if (students == null)
            {
                return NotFound();
            }

            return Json(students);
        }

        public IHttpActionResult AddClass(Class @class)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = schoolManager.CreateClass(@class);

            return Json(result);
        }

        public IHttpActionResult AddStudentToClass(AddStudentRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = schoolManager.AddStudentToClass(request.ClassId, request.Student);

            return Json(result);
        }

    }
}