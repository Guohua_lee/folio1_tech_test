﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimpleSchoolApp.Constants
{
    public static class ResponseConstants
    {
        public static string SUCCESS = "Success";
        public static string FAILURE = "Failure";
        public static string ALREADYEXISTS = "Already exists";
    }
}