﻿$(document).ready(function () {
    $("#tableClass > tbody > tr").on('click', onSelectClassRow);

    function onSelectClassRow() {
        //get row id
        var currentClassId = $(this).attr("data-key");

        //make current row active
        $("#tableClass > tbody > tr.active").removeClass("active");
        $(this).addClass("active");

        //ajax call to load student data
        $.ajax({
            url: "/api/class/" + currentClassId, success: function (result) {

            }
        });
    }

});