﻿using SimpleSchoolApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleSchoolApp.Data
{
    public interface IDataRepository
    {
        List<Class> GetAllClasses();
        Class GetClassById(int id);
        Class AddClass(Class @class);
        void DeleteClassById(int id);
        void UpdateClass(Class @class);
        Boolean CheckStudentExistsInDb(Student student);
        Student AddStudentToClass(Student student, int classId);
        List<Student> GetStudentsByClass(int classId);
        void UpdateStudent(Student student);
        void DeleteStudent(int id);
    }
}
