﻿using SimpleSchoolApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimpleSchoolApp.Data
{
    public class DataInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<SchoolContext>
    {
        protected override void Seed(SchoolContext context)
        {
            var classes = new List<Class>{
                new Class{
                    ClassName="UI/UX", 
                    Location="Building 5 Room 501", 
                    TeacherName="Mr Johnston"
                },
                new Class{
                    ClassName="DevOps", 
                    Location="Building 2 Room 606", 
                    TeacherName="Miss Thomson"
                }
            };

            classes.ForEach(c => context.Classes.Add(c));
            context.SaveChanges();

            var students = new List<Student>{
                        new Student{FirstName="John", LastName="Packer", Age=18, GPA=3.2M, ClassId=context.Classes.Single( c => c.ClassName == "UI/UX").Id},
                        new Student{FirstName="Peter", LastName="Johnston", Age=19, GPA=2.5M, ClassId=context.Classes.Single( c => c.ClassName == "UI/UX").Id},
                        new Student{FirstName="Robert", LastName="Smith", Age=20, GPA=3.1M, ClassId=context.Classes.Single( c => c.ClassName == "DevOps").Id},
                        new Student{FirstName="Louise", LastName="Thomson", Age=21, GPA=2.1M, ClassId=context.Classes.Single( c => c.ClassName == "DevOps").Id}
            };

            students.ForEach(s => context.Students.Add(s));
            context.SaveChanges();
        }
    }
}