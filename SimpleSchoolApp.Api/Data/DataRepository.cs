﻿using SimpleSchoolApp.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace SimpleSchoolApp.Data
{
    public class DataRepository: IDataRepository
    {
        private readonly SchoolContext _db = new SchoolContext();



        public List<Models.Class> GetAllClasses()
        {
            return _db.Classes.ToList();
        }

        public Class GetClassById(int id)
        {
            return _db.Classes.Find(id);
        }

        public Class AddClass(Class @class)
        {
            _db.Classes.Add(@class);
            try
            {
                _db.SaveChanges();
                return @class;
            }
            catch
            {
                throw new SystemException();
            }
        }

        public void DeleteClassById(int id)
        {
            Class @class = _db.Classes.Find(id);
            if (@class == null)
            {
                throw new ArgumentNullException();
            }

            _db.Classes.Remove(@class);
            try
            {
                _db.SaveChanges();
            }
            catch
            {
                throw new SystemException();
            }
        }

        public void UpdateClass(Class @class)
        {
            _db.Entry(@class).State = EntityState.Modified;
            try
            {
                _db.SaveChangesAsync();
            }
            catch
            {
                throw new DbUpdateConcurrencyException();
            }
        }

        public Boolean CheckStudentExistsInDb(Student student)
        {
            Student existingStudent = _db.Students.FirstOrDefault(s => s.LastName.Equals(student.LastName, StringComparison.CurrentCultureIgnoreCase));
            if (existingStudent != null)
            {
                return true;
            }

            return false;
        }

        public Student AddStudentToClass(Student student, int classId)
        {
            Class @class = _db.Classes.Find(classId);
            @class.Students.Add(student);
            try
            {
                _db.SaveChanges();
                return student;
            }
            catch
            {
                throw new SystemException();
            }
        }

        public List<Student> GetStudentsByClass(int classId)
        {
            Class @class = _db.Classes.Find(classId);
            return @class.Students.ToList();
        }

        public void UpdateStudent(Student student)
        {
            _db.Entry(student).State = EntityState.Modified;
            try
            {
                _db.SaveChanges();
            }
            catch
            {
                throw new DbUpdateConcurrencyException();
            }
        }

        public void DeleteStudent(int id)
        {
            Student student = _db.Students.Find(id);
            if (student == null)
            {
                //Todo: should use customize not found exception
                throw new SystemException();
            }

            _db.Students.Remove(student);
            try
            {
                _db.SaveChanges();
            }
            catch
            {
                throw new SystemException();
            }
        }

    }
}