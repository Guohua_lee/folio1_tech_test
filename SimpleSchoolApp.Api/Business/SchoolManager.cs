﻿using SimpleSchoolApp.Constants;
using SimpleSchoolApp.Data;
using SimpleSchoolApp.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace SimpleSchoolApp.Business
{
    public class SchoolManager
    {
        private readonly IDataRepository dbRepository;
        public SchoolManager(IDataRepository dbRepository)
        {
            this.dbRepository = dbRepository;
        }

        public IEnumerable<Class> GetClasses()
        {
            return dbRepository.GetAllClasses();
        }

        public string UpdateClass(Class @class)
        {
           
            try
            {
                dbRepository.UpdateClass(@class);
                return ResponseConstants.SUCCESS;
            }
            catch 
            {
                return ResponseConstants.FAILURE;
            }
        }

        public List<Student> GetStudentsByClass(int classId)
        {
            return dbRepository.GetStudentsByClass(classId);
        }

        public Class CreateClass(Class @class)
        {
            try
            {
                return dbRepository.AddClass(@class);
            }
            catch
            {
                return null;
            }
        }

        public string DeleteClass(int id)
        {
            try
            {
                dbRepository.DeleteClassById(id);
                return ResponseConstants.SUCCESS;
            }
            catch
            {
                return ResponseConstants.FAILURE;
            }
        }

        public Class GetClass(int id)
        {
            try
            {
               return dbRepository.GetClassById(id);
            }
            catch
            {
                return null;
            }
        }

        public AddStudentResponse AddStudentToClass(int classId, Student student)
        {
            var response = new AddStudentResponse();
            //check exists
            if(dbRepository.CheckStudentExistsInDb(student))
            {
                response.Status = ResponseConstants.ALREADYEXISTS;
                return response;
            }

            try
            {
                var result = dbRepository.AddStudentToClass(student, classId);
                if(result != null){
                    response.Student = result;
                }
                response.Status = ResponseConstants.SUCCESS;
                return response;
            }
            catch
            {
                response.Status = ResponseConstants.FAILURE;
                return response;
            }
        }

        public string UpdateStudent(Student student)
        {
            try
            {
                dbRepository.UpdateStudent(student);
                return ResponseConstants.SUCCESS;
            }
            catch
            {
                return ResponseConstants.FAILURE;
            }
        }

        public string DeleteStudent(int id)
        {
            try
            {
                dbRepository.DeleteStudent(id);
                return ResponseConstants.SUCCESS;
            }
            catch
            {
                return ResponseConstants.FAILURE;
            }
        }
    }
}