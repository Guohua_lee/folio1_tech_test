# README #

### How to run ###
* Open the solution Start the `SimpleSchoolApp.Api`
* In `SimpleSchoolApp/Web.config` change the connection string if not using the localdb
* Open `SimpleSchoolApp.Client` change the `src/Config.js` base endpoint to the api endpoint
* run `npm install` then run `npm start`


### Configuration need to run the project ###
* .Net 4.6.1
* npm or yarn to install the packages for the front-end
* In `SimpleSchoolApp/Web.config` change the connection string if not using the localdb