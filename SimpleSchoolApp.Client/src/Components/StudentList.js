import React, { Component } from 'react';
import { Table, Button, Modal } from 'react-bootstrap'
import StudentForm from './StudentForm'

class StudentList extends Component {

    constructor(props) {
        super(props)
        this.state = {
            showModel: false,
            modelContent: null
        }
    }

    handleModelClose = () => {
        this.setState({
            showModel: false
        })
    }

    handleModelOpen = () => {
        this.setState({
            showModel: true
        })
    }

    handleSubmission = (studentObj) => {
        this.props.addStudentToClass(studentObj);
        this.handleModelClose();
    }

    render() {

        const { students, isLoading } = this.props;
        var studentList = students && students.map(s => {
            return (
                <tr key={s.Id}>
                    <td className={s.GPA >= 3.2 ? "highlight": ''}>{s.FirstName} {s.LastName} {s.GPA >= 3.2 && <span className="glyphicon glyphicon-star"></span>}</td>
                    <td>{s.Age}</td>
                    <td>{s.GPA}</td>
                    <td>
                        <Button disabled>Edit</Button>
                        <Button disabled>Delete</Button>
                    </td>
                </tr>
            )
        });
        return (
            <div className="row">

                {
                    isLoading && <p>Loading ...</p>
                }
                <h2>Students in Class</h2>
                {studentList &&
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Age</th>
                                <th>GPA</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {studentList}
                        </tbody>
                    </Table>
                }
                <Button bsStyle="primary" onClick={this.handleModelOpen}>Add</Button>

                <Modal show={this.state.showModel} onHide={this.handleModelClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>{this.props.formHeading}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <StudentForm submitStudent={this.handleSubmission} />
                    </Modal.Body>
                </Modal>
            </div>
        );
    }
}

export default StudentList;