import React, { Component } from 'react';
import { FormGroup, FieldGroup, ControlLabel, FormControl, Button } from 'react-bootstrap'

class StudentForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            currentStudent: {
                FirstName: '',
                LastName: '',
                Age: 0,
                GPA: 0
            }
        }
    }

    getValidationState(value) {
        if(typeof value === 'undefined' || value == null)
            return null;
        const length = value.length;
        if (length > 0)
            return 'success';
        return null;
    }

    handleFieldOnChange = (key, value) => {
        if (typeof this.state.currentStudent[key] === 'undefined') {
            return
        }
        let currentStudentCopy = Object.assign({}, this.state.currentStudent)

        currentStudentCopy[key] = value;

        this.setState({
            currentStudent: currentStudentCopy
        })
    }

    handleFirstNameChange = (e) => {
        let key = 'FirstName';
        let value = e.target.value;
        this.handleFieldOnChange(key, value);
    }

    handleLastNameChange = (e) => {
        let key = 'LastName';
        let value = e.target.value;
        this.handleFieldOnChange(key, value);
    }

    handleAgeChange = (e) => {
        let key = 'Age';
        let value = e.target.value;
        this.handleFieldOnChange(key, value);
    }

    handleGPAChange = (e) => {
        let key = 'GPA';
        let value = e.target.value;
        this.handleFieldOnChange(key, value);
    }

    handleSubmit = () => {
        this.props.submitStudent(this.state.currentStudent)
    }

    render() {
        const { currentStudent } = this.state;
        return (
            <form>
                <FormGroup
                    controlId="txtFirstName"
                    validationState={this.getValidationState(currentStudent.FirstName)}
                >
                    <ControlLabel>First Name</ControlLabel>
                    <FormControl
                        type="text"
                        value={currentStudent.FirstName}
                        placeholder="Enter text"
                        onChange={this.handleFirstNameChange}
                    />
                    <FormControl.Feedback />
                </FormGroup>
                <FormGroup
                    controlId="txtLastName"
                    validationState={this.getValidationState(currentStudent.LastName)}
                >
                    <ControlLabel>Last Name</ControlLabel>
                    <FormControl
                        type="text"
                        value={currentStudent.LastName}
                        placeholder="Enter text"
                        onChange={this.handleLastNameChange}
                    />
                    <FormControl.Feedback />
                </FormGroup>
                <FormGroup
                    controlId="txtAge"
                    validationState={this.getValidationState(currentStudent.Age)}
                >
                    <ControlLabel>Age</ControlLabel>
                    <FormControl
                        type="number"
                        value={currentStudent.Age}
                        placeholder="Enter text"
                        onChange={this.handleAgeChange}
                    />
                    <FormControl.Feedback />
                </FormGroup>
                <FormGroup
                    controlId="txtGPA"
                    validationState={this.getValidationState(currentStudent.GPA)}
                >
                    <ControlLabel>GPA</ControlLabel>
                    <FormControl
                        type="number"
                        value={currentStudent.GPA}
                        placeholder="Enter text"
                        onChange={this.handleGPAChange}
                    />
                    <FormControl.Feedback />
                </FormGroup>
                <Button bsStyle="primary" onClick={this.handleSubmit}>Submit</Button>
            </form>
        );
    }
}

export default StudentForm;