import React, { Component } from 'react';
import { Table, Button, Modal, Tooltip, OverlayTrigger, ButtonToolbar } from 'react-bootstrap'
import ClassForm from './ClassForm'

class ClassList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showModel: false,
            modelContent: null
        }
    }

    selectClass = (id) => {
        this.props.selectClass(id);
    }

    handleModelClose = () => {
        this.setState({
            showModel: false
        })
    }

    handleModelOpen = () => {
        this.setState({
            showModel: true
        })
    }

    handleSubmission = (classObj) => {
        this.props.addNewClass(classObj);
        this.handleModelClose();
    }



    render() {
        const tooltip = (
            <Tooltip id="tooltip">
                Not implemented...
        </Tooltip>
        );
        const { classes, isLoading, currentSelectedIndex } = this.props;
        var classList = classes && classes.map(c => {
            return (
                <tr className={c.Id === currentSelectedIndex ? 'danger' : ''} key={c.Id} onClick={() => { this.selectClass(c.Id) }} >
                    <td>{c.ClassName}</td>
                    <td>{c.Location}</td>
                    <td>{c.TeacherName}</td>
                    <td>
                        <ButtonToolbar>
                            <OverlayTrigger placement="right" overlay={tooltip}>
                                <Button bsStyle="default">Edit</Button>
                            </OverlayTrigger>
                            <OverlayTrigger placement="right" overlay={tooltip}>
                                <Button bsStyle="danger" disabled>Delete</Button>
                            </OverlayTrigger>
                        </ButtonToolbar>
                    </td>
                </tr>
            )
        });
        return (
            <div className="row">
                <h2>Classes</h2>
                {
                    isLoading && <p>Loading ...</p>
                }
                {
                    classList &&

                    <Table striped bordered condensed hover>
                        <thead>
                            <tr>
                                <th>Class Name</th>
                                <th>Location</th>
                                <th>Teacher Name</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {classList}
                        </tbody>
                    </Table>
                }
                <Button bsStyle="primary" onClick={this.handleModelOpen}>Add</Button>

                <Modal show={this.state.showModel} onHide={this.handleModelClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>{this.props.formHeading}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <ClassForm submitClass={this.handleSubmission} />
                    </Modal.Body>
                </Modal>

            </div>
        );
    }
}

export default ClassList;