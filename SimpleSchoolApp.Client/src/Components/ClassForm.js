import React, { Component } from 'react';
import { FormGroup, FieldGroup, ControlLabel, FormControl, Button, Well } from 'react-bootstrap'

class ClassForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            currentClass: {
                ClassName: '',
                Location: '',
                TeacherName: ''
            },
            formInvalid: false
        }
    }

    getValidationState(value) {
        const length = value.length;
        if (length > 0)
            return 'success';
        return null;
    }

    validFormSubmission = () => {
        const currentClass = this.state.currentClass;
        if(currentClass.ClassName.length < 1){
            return false;
        }
        if(currentClass.Location.length < 1){
            return false;
        }
        if(currentClass.TeacherName.length < 1){
            return false;
        }
        return true;
    }

    handleFieldOnChange = (key, value) => {
        if (typeof this.state.currentClass[key] === 'undefined') {
            return
        }
        let currentClassCopy = Object.assign({}, this.state.currentClass)

        currentClassCopy[key] = value;

        this.setState({
            currentClass: currentClassCopy
        })
    }

    handleClassNameChange = (e) => {
        let key = 'ClassName';
        let value = e.target.value;
        this.handleFieldOnChange(key, value);
    }

    handleLocationChange = (e) => {
        let key = 'Location';
        let value = e.target.value;
        this.handleFieldOnChange(key, value);
    }

    handleTeacherNameChange = (e) => {
        let key = 'TeacherName';
        let value = e.target.value;
        this.handleFieldOnChange(key, value);
    }

    handleSubmit = () => {

        let validationResult = this.validFormSubmission();
        if(!validationResult){
            this.setState({
                formInvalid: true
            });
            return
        }
        this.props.submitClass(this.state.currentClass)
    }

    render() {
        const { currentClass } = this.state;
        return (
            <form>
                <FormGroup
                    controlId="txtClassName"
                    validationState={this.getValidationState(currentClass.ClassName)}
                >
                    <ControlLabel>Class Name</ControlLabel>
                    <FormControl
                        type="text"
                        value={currentClass.ClassName}
                        placeholder="Enter text"
                        onChange={this.handleClassNameChange}
                    />
                    <FormControl.Feedback />
                </FormGroup>
                <FormGroup
                    controlId="txtLocation"
                    validationState={this.getValidationState(currentClass.Location)}
                >
                    <ControlLabel>Location</ControlLabel>
                    <FormControl
                        type="text"
                        value={currentClass.Location}
                        placeholder="Enter text"
                        onChange={this.handleLocationChange}
                    />
                    <FormControl.Feedback />
                </FormGroup>
                <FormGroup
                    controlId="txtTeacherName"
                    validationState={this.getValidationState(currentClass.TeacherName)}
                >
                    <ControlLabel>Teacher Name</ControlLabel>
                    <FormControl
                        type="text"
                        value={currentClass.TeacherName}
                        placeholder="Enter text"
                        onChange={this.handleTeacherNameChange}
                    />
                    <FormControl.Feedback />
                </FormGroup>
                {
                    this.state.formInvalid &&
                    <Well bsSize="small" bsClass="text-danger">Please check the input</Well>               
                }
                <Button bsStyle="primary" onClick={this.handleSubmit}>Submit</Button>
            </form>
        );
    }
}

export default ClassForm;