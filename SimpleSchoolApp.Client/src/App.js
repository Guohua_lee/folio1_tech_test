import React, {
  Component
} from 'react';
import logo from './logo.svg';
import './App.css';
import ClassList from './Components/ClassList'
import StudentList from './Components/StudentList'
import Config from './Config'
import { Button } from 'react-bootstrap'


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      classes: [],
      isLoading: false,
      selectedClass: null
    }
  }

  componentDidMount() {

    const url = Config.endpoint.base + 'school/getclasses';
    this.setState({
      ...this.state,
      isLoading: true
    })
    fetch(url, {
      method: 'GET',
      headers: new Headers({
        'Content-Type': 'application/json'
      })
    }).then(res => {
      return res.json()
    }).then(data => {
      this.setState({
        ...this.state,
        classes: data,
        isLoading: false
      })
    })
  }

  handleSelectClass = (id) => {
    let selectedClass = this.state.classes.find(c => c.Id === id);
    this.setState({
      ...this.state,
      selectedClass: selectedClass
    })
  }

  addNewClass = (classObj) => {
    const url = Config.endpoint.base + 'school/AddClass';
    this.setState({
      ...this.state,
      isLoading: true
    })
    fetch(url, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json'
      }),
      body: JSON.stringify(classObj)
    }).then(res => {
      return res.json()
    }).then(data => {
      let classList = Object.assign([], this.state.classes);
      classList.push(data);
      this.setState({
        ...this.state,
        classes: classList,
        isLoading: false
      })
    })
  }

  addStudentToClass = (studentObj) => {
    const url = Config.endpoint.base + 'school/AddStudentToClass';
    this.setState({
      ...this.state,
      isLoading: true
    })
    fetch(url, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json'
      }),
      body: JSON.stringify({
        ClassId: this.state.selectedClass.Id,
        Student: studentObj
      })
    }).then(res => {
      return res.json()
    }).then(data => {
      if(data.Student){
        let selectedClassCopy = Object.assign([], this.state.selectedClass);
        selectedClassCopy.Students.push(data.Student);
        this.setState({
          ...this.state,
          selectedClass: selectedClassCopy
        })
      }

    })
  }

  render() {
    const { classes, isLoading, selectedClass } = this.state;


    let studentList = selectedClass && selectedClass.Students
    let currentSelectedClassIndex = selectedClass && selectedClass.Id

    return (
      <div className="container" >
        <ClassList
          classes={classes}
          isLoading={isLoading}
          selectClass={this.handleSelectClass}
          currentSelectedIndex={currentSelectedClassIndex}
          addNewClass={this.addNewClass}
        />
        <hr />
        {
          selectedClass && 
          <StudentList 
            students={studentList} 
            addStudentToClass={this.addStudentToClass}
          />
        }

      </div>
    );
  }
}

export default App;